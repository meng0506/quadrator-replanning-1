# CMake generated Testfile for 
# Source directory: /home/meng/catkin_ws/src/ewok/ewok_optimization
# Build directory: /home/meng/catkin_ws/build/ewok/ewok_optimization
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_ewok_optimization_gtest_test_uniform_bspline_3d_optimization "/home/meng/catkin_ws/build/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/home/meng/catkin_ws/build/test_results/ewok_optimization/gtest-test_uniform_bspline_3d_optimization.xml" "--return-code" "/home/meng/catkin_ws/devel/lib/ewok_optimization/test_uniform_bspline_3d_optimization --gtest_output=xml:/home/meng/catkin_ws/build/test_results/ewok_optimization/gtest-test_uniform_bspline_3d_optimization.xml")
