# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/meng/catkin_ws/src/ewok/ewok_simulation/src/bspline_lee_position_controller_node.cpp" "/home/meng/catkin_ws/build/ewok/ewok_simulation/CMakeFiles/bspline_lee_position_controller_node.dir/src/bspline_lee_position_controller_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"ewok_simulation\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/eigen3"
  "/usr/include/suitesparse"
  "/home/meng/catkin_ws/src/ewok/ewok_simulation/include"
  "/home/meng/catkin_ws/devel/include"
  "/home/meng/catkin_ws/src/ewok/rotors_simulator/rotors_control/include"
  "/home/meng/catkin_ws/src/ewok/mav_comm/mav_msgs/include"
  "/home/meng/catkin_ws/src/ewok/ewok_poly_spline/include"
  "/home/meng/catkin_ws/src/ewok/ewok_optimization/include"
  "/home/meng/catkin_ws/src/ewok/ewok_ring_buffer/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/opt/ros/melodic/share/orocos_kdl/cmake/../../../include"
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/meng/catkin_ws/build/ewok/rotors_simulator/rotors_control/CMakeFiles/lee_position_controller.dir/DependInfo.cmake"
  "/home/meng/catkin_ws/build/ewok/rotors_simulator/rotors_control/CMakeFiles/roll_pitch_yawrate_thrust_controller.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
